record(stringin, "$(P)$(R)Model")
{
    field(DESC, "get device model name")
    field(DTYP, "stream")
    field(INP,  "@minicircuits_rfswitch.proto cmd_model() $(PORT)")
    field(PINI, "YES")
}

record(stringin, "$(P)$(R)Serial")
{
    field(DESC, "get device serial")
    field(DTYP, "stream")
    field(INP,  "@minicircuits_rfswitch.proto cmd_serial() $(PORT)")
    field(PINI, "YES")
}

record(stringin, "$(P)$(R)FwVersion")
{
    field(DESC, "get device firmware version")
    field(DTYP, "stream")
    field(INP,  "@minicircuits_rfswitch.proto cmd_fwversion() $(PORT)")
    field(PINI, "YES")
}

record(longout, "$(P)$(R)Connect") {
    field(DESC, "Set which port is connected to COM")
    field(DTYP, "stream")
    field(OUT,  "@minicircuits_rfswitch.proto set_switch_state() $(PORT)")
    field(DRVL, "0")
    field(DRVH, "16")
    field(FLNK, "$(P)$(R)Status")
}

record(longin, "$(P)$(R)Status") {
    field(DESC, "Read which port is connected to COM")
    field(DTYP, "stream")
    field(INP,  "@minicircuits_rfswitch.proto get_switch_state() $(PORT)")
    field(PINI, "YES")
}

###################################################################################

record(asyn, "$(P)$(R)AsynIO")
{
    field(DTYP, "asynRecordDevice")
    field(PORT, "$(PORT)")
}

record(bi, "$(P)$(R)AsynConnectedR")
{
    field(DESC, "Connection state")
    field(INP,  "$(P)$(R)AsynIO.CNCT CP")
    field(ONAM, "Connected")
    field(ZNAM, "Disconnected")
    field(ZSV,  "MAJOR")
}
